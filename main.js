//config
{
  var walkerxdirection = "right";
  var level = 0;
  var points = 0;
  var timer = 0;
  var levelReqPnts = 0;
  var levelReqPntsSet = false;
}
//sprites
{
  //walker
  {
    var walker = createSprite(0,200,100,100);
    walker.setAnimation("walker.gif");
  }
  //flier
  {
    var flier = createSprite(200,200,20,20);
    flier.setAnimation("flier.gif");
    flier.visible = false;
  }
  //coin1
  {
    var coin1 = createSprite(200,200);
    var coin1Counted = false;
    coin1.setAnimation("coin.gif");
    coin1.visible = false;
    coin1.setCollider("circle");
    coin1.scale = 0.5;
  }
  //coin2
  {
    var coin2 = createSprite(200,200);
    var coin2Counted = false;
    coin2.setAnimation("coin.gif");
    coin2.visible = false;
    coin2.setCollider("circle");
    coin2.scale = 0.5;
  }
  //coin3
  {
    var coin3 = createSprite(200,200);
    var coin3Counted = false;
    coin3.setAnimation("coin.gif");
    coin3.visible = false;
    coin3.setCollider("circle");
    coin3.scale = 0.5;
  }
  //coin4
  {
    var coin4 = createSprite(200,200);
    var coin4Counted = false;
    coin4.setAnimation("coin.gif");
    coin4.visible = false;
    coin4.setCollider("circle");
    coin4.scale = 0.5;
  }
}

//draw loop
function draw() {
  if (level === 0) {
    makeLevel(0);
  } else if (level >= 1) {
    levelInfo();
  }
}

//levels
function levelInfo() {
  if (level === 1) levelReqPnts = 1;
  if (level === 2) levelReqPnts = 3;
  if (level === 3) levelReqPnts = 6;
  if (level >= 4 && levelReqPntsSet === false) {
    levelReqPnts = levelReqPnts + 4;
    levelReqPntsSet = true;
  }
  makeLevel(level, levelReqPnts);
}

//create level
function makeLevel(currentLevel, requiredPoints) {
  if (currentLevel === 0 && requiredPoints === undefined) {
    background(rgb(29, 57, 102));
    //change direction
    if (walker.x < 48 && walkerxdirection === "left") {
      walkerxdirection = "right";
    } else if (walker.x > 352 && walkerxdirection === "right") {
      walkerxdirection = "left";
    }
    //move the walker
    if (walkerxdirection === "right") {
      walker.x = walker.x + 1;
    } else if (walkerxdirection === "left") {
      walker.x = walker.x - 1;
    }
    fill("white");
    textSize(20);
    //message
    if (timer < 100) {
      text("Time to walk", 125, 300);
    } else if (timer < 200 && timer > 100) {
      text("What should I do out here?", 100, 300);
    } else if (timer < 300 && timer > 200) {
      text("Maybe I'll just walk around.", 50, 300);
    } else if (timer < 400 && timer > 300) {
      text("Or maybe I'll fly!", 40, 300);
      walker.setAnimation("flier.gif");
    } else if (timer < 500 && timer > 400) {
      background("white");
      walker.visible = false;
    }
    timer++;
    if (timer > 500) {
      level++;
      timer = 0;
    }
    //draw sprites
    drawSprites();
  } else if (currentLevel > 0) {
    levelReqPnts = requiredPoints;
    sceneSetup();
    coins(currentLevel);
    progressLevel(currentLevel);
    drawSprites();
  }
}

//create coins
function coins(currentLevel) {
  if (currentLevel === 1) {
    makeCoin(1);
  } else if (currentLevel === 2) {
    makeCoin(1);
    makeCoin(2);
  } else if (currentLevel === 3) {
    makeCoin(1);
    makeCoin(2);
    makeCoin(3);
  } else if (currentLevel >= 4) {
    makeCoin(1);
    makeCoin(2);
    makeCoin(3);
    makeCoin(4);
  }
}

//level progression
function progressLevel(currentLevel) {
  if (flier.y < 0) {
    level = currentLevel + 1;
    flier.x = 200;
    flier.y = 200;
    coin1Counted = false;
    coin2Counted = false;
    coin3Counted = false;
    coin4Counted = false;
    levelReqPntsSet = false;
  }
}

//scene setup
function sceneSetup() {
  //setup
  flier.width = 75;
  flier.height = flier.width;
  flier.visible = true;
  background(rgb(117, 170, 255));
    
  //clouds
  fill("white");
  noStroke();
  ellipse(0,350,400,50);
  ellipse(200,350,400,50);
  ellipse(400,350,400,50);
  ellipse(0, 50, 400, 50);
  ellipse(200, 50, 400, 50);
  ellipse(400, 50, 400, 50);
    
  //points and level counter
  fill("brown");
  rect(0,0,60,30);
  fill("white");
  text("Level: "+level, 3, 15);
  text("Points: "+points, 3, 25);
  
  //move flier
  if ((keyDown("left") || keyDown("a")) && flier.x > 0) {
    flier.velocityX = -5;
    flier.velocityY = 0;
  } else if ((keyDown("right") || keyDown("d")) && flier.x < 400) {
    flier.velocityX = 5;
    flier.velocityY = 0;
  } else if ((keyDown("up") || keyDown("w")) && (flier.y > 85 || points === levelReqPnts)) {
    flier.velocityY = -4.5;
    flier.velocityX = 0;
  } else if ((keyDown("down") || keyDown("s")) && flier.y < 300) {
    flier.velocityY = 5;
    flier.velocityX = 0;
  } else {
    flier.velocityX = 0;
    if (flier.y < 300) {
      flier.velocityY = 0.5;
    } else {
      flier.velocityY = 0;
    }
  }
}

//make coins
function makeCoin(coinToMake) {
  if (coinToMake === 1) {
    if (coin1.visible === false && coin1Counted === false) {
      coin1.visible = true;
      coin1.x = randomNumber(0,400);
      coin1.y = randomNumber(60, 300);
    } else if (coin1Counted === true) {
      coin1.visible = false;
    }
    if (flier.collide(coin1)) {
      points++;
      coin1.visible = false;
      coin1Counted = true;
      coin1.x = 500;
      coin1.y = 500;
    }
  } else if (coinToMake === 2) {
    if (coin2.visible === false && coin2Counted === false) {
      coin2.visible = true;
      coin2.x = randomNumber(0,400);
      coin2.y = randomNumber(60, 300);
    } else if (coin2Counted === true) {
      coin2.visible = false;
    }
    if (flier.collide(coin2)) {
      points++;
      coin2.visible = false;
      coin2Counted = true;
      coin2.x = 500;
      coin2.y = 500;
    }
  } else if (coinToMake === 3) {
    if (coin3.visible === false && coin3Counted === false) {
      coin3.visible = true;
      coin3.x = randomNumber(0,400);
      coin3.y = randomNumber(60, 300);
    } else if (coin3Counted === true) {
      coin3.visible = false;
    }
    if (flier.collide(coin3)) {
      points++;
      coin3.visible = false;
      coin3Counted = true;
      coin3.x = 500;
      coin3.y = 500;
    }
  } else if (coinToMake === 4) {
    if (coin4.visible === false && coin4Counted === false) {
      coin4.visible = true;
      coin4.x = randomNumber(0,400);
      coin4.y = randomNumber(60, 300);
    } else if (coin4Counted === true) {
      coin4.visible = false;
    }
    if (flier.collide(coin4)) {
      points++;
      coin4.visible = false;
      coin4Counted = true;
      coin4.x = 500;
      coin4.y = 500;
    }
  }
}